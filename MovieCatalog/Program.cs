﻿using System;
using System.IO;
using CsvHelper;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;

namespace MovieCatalog
{
    class Program
    {
        public static void Main(string[] args)
        {
            List<Movie> movie = new List<Movie>();
            bool stop = false;

            do
            {
                Console.WriteLine("Enter a movie name: ");
                string name = Console.ReadLine();
                Console.WriteLine("Enter a release year for that movie: ");
                int year = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter that movies genre: ");
                string genre = Console.ReadLine();

                //List<Movie> movie = new List<Movie>();
                movie.Add(new Movie() { Name = name, Year = year, Genre = genre });
                WriteMovie(movie);

                Console.WriteLine("Do you want to stop?(y/n)");
                string close = Console.ReadLine();
                if(close == "y") { stop = true; }
            }
            while (stop == false);

            Console.WriteLine(ReadMovie());
        }
        public static void WriteMovie(List<Movie> movie)
        {
            //movie.Add(new Movie() { Name = name, Year = release, Genre = genre });

            try
            {
                using (StreamWriter writer = new StreamWriter("MovieTable.csv"))
                {
                    using (CsvWriter csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                    {
                        csv.WriteRecords<Movie>(movie);
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public static string ReadMovie()
        {
            try
            {
                using(StreamReader reader = new StreamReader("MovieTable.csv"))
                {
                    using(CsvReader csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                    {
                        IEnumerable<Movie> movies = csv.GetRecords<Movie>();

                        foreach(Movie movie in movies)
                        {
                            return movie.Name+","+Convert.ToString(movie.Year)+","+movie.Genre;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
            return "==============";
        }
    }
}
