﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieCatalog
{
    public class Movie
    {
        public string Name { get; set; }
        public int Year { get; set; }
        public string Genre { get; set; }
    }
}
